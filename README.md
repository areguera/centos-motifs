> ⛔ Development of this project is now at: https://gitlab.com/CentOS/artwork/centos-motif

# CentOS Stream 9 Artistic Motif

![CentOS Stream 9 Artistic Motif](preview.png)

## Author

Alain Reguera Delgado, 2021.

## License

This is a free work, you can copy, distribute, and modify it under the terms of
the Free Art License https://artlibre.org/licence/lal/en/.
